import sys
import json
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Arrow, Circle



def drawUpArrow(x, y):
    # TODO - implement drawUpArrow here
    print("TODO - implement drawUpArrow")

    

def main():
    if len(sys.argv) < 2:
        print("Missing argument.")
        print("Usage: testing.py <JSON file>")
        sys.exit(1)
    json_file = sys.argv[1]
    with open(json_file) as data_file:
        data = json.load(data_file)
    tasks = data["tasks"]
    max_t = data["max_t"]
    executions = data["executions"]
    
    
    
    ax = plt.axes()


    # Arrow from matplotlib.pyplot
    ax.arrow(0, 0, 2, 1, head_width=0.5, head_length=0.5, facecolor='black', edgecolor='black')

    # Draw a line from (0,0) to (3,10)
    plt.plot([0,3],[0,10])

    plt.text(0,3,"hello")

    # Set some colors to use
    colors=["blue","red","green","#2f627a","aliceblue","orange"]

    # Circle from matplotlib.patches
    circle = Circle((5,5), 4, color=colors[3])
    ax.add_patch(circle)

    # Arrow from matplotlib.patches
    arrow = Arrow(2, 2, 1, 3, width=0.2, color=colors[1])
    ax.add_patch(arrow)




    # Make line for each task
    # TODO

    # Draw arrival times and deadlines
    # TODO

    # Draw each execution
    # TODO

    
    ax.autoscale_view()
    plt.axis('off')

    plt.savefig('test.pdf', bbox_inches='tight')

main()
