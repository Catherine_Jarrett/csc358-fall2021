import json
import sys

def addExecution(out_data, task, job, start, end):
    print("TODO")

def main():
    if len(sys.argv) < 3:
        print("Missing argument.")
        print("Usage: EDF.py <input.json> <output.json>")
        sys.exit(1)

    # Read input data
    with open(sys.argv[1]) as in_file:    
        in_data = json.load(in_file)
    tasks = in_data["tasks"]
    max_t = in_data["max_t"]
    
    # Create and initialize dictionary for output data
    out_data = {}
    out_data['max_t'] = max_t

    # Create empty list of executions
    out_data['executions'] = []

    # Add a sample execution - TODO - remove this example
    sample = {}
    sample['taskNum'] = 0
    sample['jobNum'] = 0
    sample['start'] = 0
    sample['end'] = 5
    out_data['executions'].append(sample)



    with open(sys.argv[2], 'w') as out_file:  
        json.dump(out_data, out_file)

main()
